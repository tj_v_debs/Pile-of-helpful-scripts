#! /bin/bash

function func_clone () {
	echo "SOURCE: $var_source | TARGET: $var_target"
	echo "1: Select Source disk"
	echo "2: Select target disk"
	echo "3: Begin cloning!" 
	echo "4: Return to main menu"
	
	read var_n
		case $var_n in
			1) 	lsblk
				echo "Please select source disk"
					read var_source
					echo "$var_source selected as source"
				read -rsn1 -p"Press any key to continue";echo
				func_clone;;
				
			2)	lsblk
				echo "Please select target disk"
					read var_target
					echo "$var_target selected as target disk!"
					func_clone;;
					
			3) echo "Are you sure you wish to clone $var_source over $var_target? (Y/N)"
				echo "WARNING: CONTENTS OF $VAR_TARGET WILL BE LOST!!!!"
				read var_confirmation
					if [[ $var_confirmation == Y ]]; then
						dd if="/dev/$var_source" of="/dev/$var_target" conv=sync,noerror bs=64K status=progress
					else
						echo "Returning to menu!"
						read -rsn1 -p"Press any key to continue";echo
						func_clone
					fi
					;;
			4) echo "returning to main menu!"
					read -rsn1 -p"Press any key to continue";echo
					func_greeter;;
			esac
}
					
function func_backup () {
	echo " BACKUP FUNCTION!" 
	echo "SOURCE DISK: $var_source | TARGET: $var_target/$var_target_name"
	echo "1: Select Source"
	echo "2: Select target location and name"
	echo "3: Begin Backup!"
	echo "4: Main menu"
	read var_n
		case $var_n in
			1) lsblk
				echo "please select a source disk or partition for backing up"
					read var_source
				echo "$var_source chosen!"
					func_backup;;
			2) echo "Please select a directory to save your backup to"
					read var_target
						echo "$var_target chosen!"
				
				echo "Please select a name for your backup image!"
					read var_target_name
				echo "$var_target_name chosen!"
				func_backup;;
			
			3) echo "Starting backup process!"
				echo "Backing up $var_source to $var_target/$var_target_name"
				echo "Is this correct? (Y/N)"
					read var_confirmation
						if [[ $var_confirmation == Y ]]; then
							 dd if="/dev/$var_source" conv=sync,noerror bs=64K status=progress | gzip -c > $var_target/$var_target_name.img.gz
								echo "$var_source copied to $var_target/$var_target_name!"
									echo "Continue using dd-helper? (Y/N)"
										read var_confirmation_2
											if [[ $var_confirmation_2 == Y ]]; then
												echo "Returning to main menu!"
												func_greeter
											else
												exit
											fi
						else
							echo "Returning to menu!"
								func_backup
						fi;;
						
			4) echo "Returning to main menu!"
					func_greeter;;
			
			*) echo "invalid operation!"
				func_backup;;
			
			esac
}


function func_restore () {
	echo "Restore function!"
	echo "SOURCE: $var_source | TARGET: $var_target"
	echo "1: Select source image!"
	echo "2: Select target device!"
	echo "3: Begin restoration!"
	echo "4: Return to main menu!"
	
	read var_n
		case $var_n in
			1) echo "Please select source image"
					read var_source
						echo "$var_source chosen!"
				func_restore;;
			
			2) 	lsblk
				echo "Please select target device!"
					read var_target
						echo "$var_target chosen!"
				func_restore;;
			
			3) echo "Restoring $var_source to /dev/$var_target"
				echo "WARNING: ALL DATA ON $var_target WILL BE LOST!!!!"
					echo "Are you sure this is correct? (Y/N)"
						read var_confirmation
							if [[ $var_confirmation == Y ]]; then
									if [[ $var_source =~ \.img.gz ]]; then 
										sudo gzip -cd $var_source | sudo dd of=/dev/$var_target status=progress
									else
										sudo dd if=$var_source of=/dev/$var_target bs=16K status=progress
									fi
									
							else
								echo "returning to menu!"
									func_restore
							fi
							;;
			4) echo "returning to main menu!"
				func_greeter;;
			
			*) echo "Invalid operation!"
				func_restore;;
			esac
}
					
function func_greeter () {
	echo "Hello! Thanks for using Version 2 of TJ's DD helper"
	echo "Please select a function!"
	echo "1: Clone Disk"
	echo "2: Backup disk"
	echo "3: Restore Backup/Image"
	echo "4: Quit"
		read var_choice
		case $var_choice in
				1) 	echo "clone function chosen!"
					read -rsn1 -p"Press any key to continue";echo
					func_clone;;
				
				2) echo "Backup function chosen!"
					read -rsn1 -p"Press any key to continue";echo
					func_backup;;
					
				3) echo "Restore function chosen!"
					read -rsn1 -p"Press any key to continue";echo
					func_restore;;
				
				4) echo "Quitting operations!"
						exit;;
		esac 
}

func_greeter