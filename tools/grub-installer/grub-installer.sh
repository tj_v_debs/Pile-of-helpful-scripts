#! /bin/bash

#TJ's grub-installer


function func_disk () {

lsblk
echo "please select your boot disk!"
	read var_boot_disk
	echo "installing grub to $var_boot_disk"
		grub-install --target=i386-pc /dev/$var_boot_disk
	echo "Grub Installed, returning to main menu!"
	 read -rsn1 -p"Press any key to continue";echo
		func_main_menu
}

function func_main_menu () {

echo "**************TJ's grub install helper!*********"
echo "MAIN MENU"
echo "Please select an option"
echo "1:Install grub for MBR system"
echo "2:Install grub for EFI system"
echo "3:Edit grub config"
echo "4:Generate grub.cfg"
echo "5:Quit."
	read n
	case $n in

		1) echo "Installing grub for MBR system!"				
			func_disk;;
	
		2) echo "NOT YET IMPLEMENTED!"
			func_main_menu;;

		3) echo "editing grub.cfg!"
			nano /etc/default/grub
			func_main_menu;;

		4) echo "Generating grub.cfg!"
			grub-mkconfig -o /boot/grub/grub.cfg
			echo "Done!"
			func_main_menu;;
		5) echo "Quitting :("
			exit;;

		*) echo "invalid option"
			func_main_menu;;
	esac
}

func_main_menu
