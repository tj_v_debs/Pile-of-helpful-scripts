#TJ's Fixes

#Just a bunch of different fixes I've come across while using arch. My memory is garbage so any time I fix anything
# I immediately write a script that either automates the fix or explains how to repeat it.

#TABLE OF CONTENTS
#	MAIN MENU - (MAIN00)
# 	TOOLS MENU - (TOOLS00)	
#	XORG RELATED FIXES - (XORG00)
#		PROTOCOL PERMISSIONS FIXER - (XORG01) OR (XPROTOCOL00)

#PROTOCOL PERMISSION FIXER (XORG01) (XPROTOCOL00)
#Fixes permission errors for /lib/xorg/protocol.txt

#GRUB MIGRATE (GRUBM00)
function func_grub_disk () {

lsblk
echo "please select your boot disk!"
	read var_boot_disk
	echo "installing grub to $var_boot_disk"
		grub-install --target=i386-pc /dev/$var_boot_disk
	echo "Grub Installed, returning to main menu!"
	 read -rsn1 -p"Press any key to continue";echo
		func_grub_menu
}



function func_grub_menu () {

echo "**************TJ's grub install helper!*********"
echo "MAIN MENU"
echo "Please select an option"
echo "1:Install grub for MBR system"
echo "2:Install grub for EFI system"
echo "3:Edit grub config"
echo "4:Generate grub.cfg"
echo "5:Quit."
	read n
	case $n in

		1) echo "Installing grub for MBR system!"				
			func_grub_disk;;
	
		2) echo "NOT YET IMPLEMENTED!"
			func_grub_menu;;

		3) echo "editing grub.cfg!"
			nano /etc/default/grub
			func_grub_menu;;

		4) echo "Generating grub.cfg!"
			grub-mkconfig -o /boot/grub/grub.cfg
			echo "Done!"
			func_grub_menu;;
		5) echo "Back to migration helper!"
			func_migration;;

		*) echo "invalid option"
			func_grub_menu;;
	esac
}

#MIGRATION HELPER FUNCTION (MIGRATE00)
# helps the user move arch installs from pc to pc or in and out of a vm.

function func_migration () {
echo "MIGRATION MENU!"
echo "In order to transition to new hardware you must regen the fstab, reinstall and reconfigure grub, then regen the kernel!"
echo "Lets begin!" 
echo "1: Regen Fstab"
echo "2: arch-chroot into /mnt"
echo "3: Reinstall and Reconfigure Grub"
echo "4: Regen Kernel"
echo "5: All done:)!"

read var_m
	case $var_m in

	1) func_regen_fstab
		 func_migration
			set $var_fstab_status == true;;
			
	2) echo "arch-chrooting into /mnt/"
		echo "Please rexecute script"
		read -rsn1 -p"Press any key to continue";echo
		arch-chroot /mnt;;

	3) echo "Grub reinstallation/reconfiguration selected!"
		func_grub_menu
		set $var_grub_status == done;;
		
	4) echo "Regenerating Kernel!"
		mkinitcpio -p linux
		set $var_kernel_status == complete
		func_migration;;
	4) echo "quitting!"
		exit;;
		
	esac
	}

	function func_regen_fstab () {

lsblk
			echo "Please make sure your mount points are correct.(Y/N)"
				read var_confirm
					case $var_confirm in
						Y) echo "Regenerating fstab"
            rm /mnt/etc/fstab
            genfstab -U /mnt >> /mnt/etc/fstab
						set $var_fstab_status == true
						echo "Please rerun this script as chroot." 
						read -rsn1 -p"Press any key to continue";echo
						func_migration;;
						
						N) echo "Please correct mount points then rerun this script."
							lsblk
								exit;;
						*) echo "Invalid selection, please select Y or N"
						func_migration;;
					esac

}
		
		



		
function func_protocolpermission {

echo "First thing to try is making root the owner of protocol.txt."
#echo the startx command can occasionally mess up the permissions for /lib/xorg/protocol.txt by setting the owner as the
#user rather than root"
echo "The startx command can occasionally mess up the permissions for /lib/xorg/protocol.txt by setting the owner as the"
echo "User rather than root which can cause the xserver to fail to load properly, weird desktop bugs, or a black screen."

echo "This is, I think, due to startx being ran as a user rather than root."
#echo "Running startx as a superuser may fix this, but you typically do not want to start an xserver as a superuser because
echo "Running startx as a superuser may fix this, but you normally do not want to start an xserver as a superuser because:"
echo "1: All your profile configurations including desktop settings and .config files will not apply to the root home folder"
echo "2: It may not actually fix the issue because it does not change the ownership of the protocol.txt"
echo "This script can try to fix this problem by resetting the ownership to root"
echo "Would you like me to try this? (Y/N)?" 
	read var_protocolownership
		case $var_protocolownership in
			yes) echo "attempting repairs"
				sudo chown root /lib/xorg/protocol.txt;;
			y) echo "attempting repairs"
				sudo chown root /lib/xorg/protocol.txt;;
      Y)  echo "attempting repairs"
				sudo chown root /lib/xorg/protocol.txt;;
      YES)  echo "attempting repairs"
				sudo chown root /lib/xorg/protocol.txt;;
			n) echo "canceling operation"
				#May replace this later with a return to main menu as more fixes are created. ADDON00
				func_main_menu;;
			no) echo "canceling operation"
				func_main_menu;;
      N) echo "canceling operation"
				func_main_menu;;
			*) echo "invalid entry"
          func_protocolpermission;;
		esac
}



#XORG SUBMENU (XORG00)
function func_xorg_menu {
echo "Welcome to the Xorg catagory of my fixes script!"
	echo "Please select a fix"
	echo "1:Protocol permission fixer"
	echo "2:RESERVED"
	echo "3:RESERVED"
	echo "R:Return to main menu"
		read var_submenu 
		case $var_submenu in
			1) echo "Protocol permission fixer selected!"
				read -rsn1 -p"Press any key to continue";echo
				func_protocolpermission;;
			R) echo "Returning to main menu."
		   		read -rsn1 -p"Press any key to continue";echo
				func_main_menu;;
				
			r) echo "Returning to main menu."
		   		read -rsn1 -p"Press any key to continue";echo
				func_main_menu;;				

			*) echo "invalid operation"
				read -rsn1 -p"Press any key to continue";echo
				func_xorg_menu;;
		esac
}

#TOOLSMENU (TOOLS00)
#Useful tools I've made as well as tools made by others. 

function func_tools_menu {
#variables section for various scripts and tools

	echo "You have selected the tools menu!"
	echo "Please select a tool you would like to use"
	echo "1: TJ's DD Helper"
	echo "2: TJ's Virtualbox helper"
	echo "3: Grub Install Helper"
	echo "4: Migration Helper!"
  echo "5: Arch Wiki Downdloader"
	echo "R: Return to main menu"
	
	read var_tool
		case $var_tool in
		
			1) 	echo "TJ's DD Helper selected!"
				read -rsn1 -p"Press any key to continue";echo
				sudo bash ./tools/dd-helper-v2/dd-helper-v2.sh;;

			2) echo "TJ's Virtualbox helper selected!"
			   read -rsn1 -p"Press any key to continue";echo
			   sudo bash ./tools/vboxdiskhelper/vbox-helper.sh;;
			
			3) echo "Grub Install helper Selected!"
				./tools/grub-installer/grub-installer.sh;;
				
			4) echo "Hardware Migiration helper selected!"
					func_migration;;
                          
      5) echo "Arch-Wiki-Downloader Selected!"
          func_archdl;;
				
			R) echo "Returning to main menu"
				read -rsn1 -p"Press any key to continue";echo
				func_main_menu;;
				
			r) echo "Returning to main menu"
				read -rsn1 -p"Press any key to continue";echo
				func_main_menu;;			
			
			*) echo "invalid input"
				func_tools_menu;;
			
		esac

}

#MAIN MENU (MAIN00)
#just the basic main menu that calls other functions
function func_main_menu {
	echo "Hello! Thank you for using TJ's helpful fixes!"
	echo "Please select a problem catagory"
	echo " OR type 'explain' for a more detailed explanation as to the purpose of these scripts"
	echo "1: xorg and other X11 related errors"
	echo "2: RESERVED"
	echo "3: RESERVED"
	echo "4: RESERVED"
	echo "T: TOOLS"
	read var_main_menu_catagory
		case $var_main_menu_catagory in
			1)echo "Xorg fixes chosen!" 
			func_xorg_menu ;;
			
			explain) cat ./mainmenu_explanation.txt
				cat ./LINEBREAK.txt
			
				read -rsn1 -p"Press any key to continue";echo
				func_main_menu;;

			T) echo "Tools selected!"
				read -rsn1 -p"Press any key to continue";echo
				func_tools_menu;;
				
			t) echo "Tools selected!"
				read -rsn1 -p"Press any key to continue";echo
				func_tools_menu;;
					 
			 *) echo "invalid input";;
		esac
}


function func_archdl () {
echo "Would you like to download the entire arch wiki? (Y/N)"
	read var_n
		case $var_n in
	Y) echo "downloading arch wiki"
		 read -rsn1 -p "Press any key to continue";echo
		 wget -mkEpnp https://wiki.archlinux.org/;;
      
      y) echo "download arch wiki"
			  read -rsn1 -p "Press any key to continue";echo
				wget -mkEpnp https://wiki.archlinux.org/;;
      
      yes) echo "downloading arch wiki"
			  read -rsn1 -p "Press any key to continue";echo
				wget -mkEpnp https://wiki.archlinux.org/;;
      
      Yes) echo "downloading arch wiki"
            read -rsn1 -p "Press any key to continue";echo
				    wget -mkEpnp https://wiki.archlinux.org/;;
                             
      YES) echo "downloading arch wiki"
            read -rsn1 -p "Press any key to continue";echo
				    wget -mkEpnp https://wiki.archlinux.org/;;
      
      n)   echo "canceling"
            read -rsn1 -p "Press any key to continue";echo
            func_main_menu;;
      
      N)     echo "canceling"
            read -rsn1 -p "Press any key to continue";echo
            func_main_menu;;
      
      No)     echo "canceling"
            read -rsn1 -p "Press any key to continue";echo
            func_main_menu;;
      
      NO)     echo "canceling"
            read -rsn1 -p "Press any key to continue";echo
            func_main_menu;;
      
      no)   echo "canceling"
            read -rsn1 -p "Press any key to continue";echo
            func_main_menu;;
      
     esac
 
}	


func_main_menu