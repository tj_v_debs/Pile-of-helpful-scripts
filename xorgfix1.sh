#!/bin/bash

#First attempt at fixing xorg
#sets root as  owner of /lib/xorg/protocol.txt
#startx occasionally fucks up the permissions for protocol.txt by setting the ownership to the user rather than root

function func_protocolpermission {

echo "First thing to try is making root the owner of protocol.txt."
#echo the startx command can occasionally mess up the permissions for /lib/xorg/protocol.txt by setting the owner as the
#user rather than root"
echo "The startx command can occasionally mess up the permissions for /lib/xorg/protocol.txt by setting the owner as the"
echo "User rather than root which can cause the xserver to fail to load properly, weird desktop bugs, or a black screen."

echo "This is, I think, due to startx being ran as a user rather than root."
#echo "Running startx as a superuser may fix this, but you typically do not want to start an xserver as a superuser because
echo "Running startx as a superuser may fix this, but you normally do not want to start an xserver as a superuser because:"
echo "1: All your profile configurations including desktop settings and .config files will not apply to the root home folder"
echo "2: It may not actually fix the issue because it does not change the ownership of the protocol.txt"
echo "This script can try to fix this problem by resetting the ownership to root"
echo "Would you like me to try this? (Y/N)?" 
	read var_protocolownership
		case $var_protocolownership in
			yes) echo "attempting repairs"
				sudo chown root /lib/xorg/protocol.txt;;
			y) echo "attempting repairs"
				sudo chown root /lib/xorg/protocol.txt;;
			n) echo "canceling operation"
				#May replace this later with a return to main menu as more fixes are created. ADDON00
				exit;;
			no) echo "canceling operation"
				exit;;
			*) echo "invalid entry";;
		esac
}

func_protocolpermission
